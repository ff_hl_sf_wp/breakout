package com.breakOut.main;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

import com.breakOut.constants.ブロック配置パターン;
import com.breakOut.constants.定数情報;
import com.breakOut.control.バーの制御;
import com.breakOut.control.ブロックの制御;
import com.breakOut.control.ボールの制御;
import com.breakOut.control.背景の制御;
import com.breakOut.enums.ゲームの進行;
import com.breakOut.enums.ベクトルの種類;
import com.breakOut.enums.移動方向の種類;
import com.breakOut.model.バー;
import com.breakOut.model.ブロック;
import com.breakOut.model.ボール;
import com.breakOut.model.管理情報;

public class ブロック崩し extends Applet implements Runnable, KeyListener {

	private static final long serialVersionUID = -3456508761664467043L;

	private 管理情報 管理情報 = null;

	private ボール ボール情報 = null;

	private バー バー情報 = null;

	private List<List<ブロック>> ブロック段情報リスト = null;;

	@Override
	public void init() {
		初期化();
		addKeyListener(this);
	}

	/**
	 * 初期化
	 */
	public void 初期化() {

		管理情報 = null;
		ボール情報 = null;
		バー情報 = null;

		管理情報 = new 管理情報();
		ボール情報 = new ボール();
		バー情報 = new バー();

		管理情報の初期化();
		ブロックの定義();
		ボールの制御.初期化(ボール情報, 管理情報);
		バーの制御.初期化(バー情報, 管理情報);
	}

	/**
	 * 再起動
	 */
	public void 再起動() {
		初期化();
	}

	/**
	 * 管理情報の初期化
	 */
	private void 管理情報の初期化() {
		管理情報.ゲームの進行情報 = ゲームの進行.起動;
		管理情報.表示領域 = new Dimension(定数情報.画面の幅, 定数情報.画面の高さ);
		管理情報.バッファ = createImage(管理情報.表示領域.width, 管理情報.表示領域.height);
		管理情報.グラフィック = 管理情報.バッファ.getGraphics();

		管理情報.移動範囲_幅 = 管理情報.表示領域.width - (定数情報.マージン * 2);
		管理情報.移動範囲_高さ = 管理情報.表示領域.height - (定数情報.マージン * 2);
		管理情報.左の移動制限X = 定数情報.マージン;
		管理情報.上の移動制限Y = 定数情報.マージン;
		管理情報.右の移動制限X = 定数情報.マージン + 管理情報.移動範囲_幅 - 定数情報.ボールの幅;
		管理情報.下の移動制限Y = 管理情報.移動範囲_高さ - 定数情報.マージン;
		管理情報.クリアありか = false;
	}

	/**
	 * ブロックの定義
	 */
	private void ブロックの定義() {

		ブロック段情報リスト = null;

		// FIXME
		ブロック段情報リスト = ブロック配置パターン.パターン１();

		ブロックの制御.座標設定(管理情報, ブロック段情報リスト);
		ブロックの制御.ブロックの衝突範囲の設定(管理情報, ブロック段情報リスト);
	}

	@Override
	public void paint(Graphics g) {
		update(g);
	}

	@Override
	public void update(Graphics g) {

		背景の制御.描写(管理情報);

		switch (管理情報.ゲームの進行情報) {
		case 起動:
			ゲーム開始を描く();
			break;
		case ゲーム中:
			ボールの制御.制御(ボール情報, バー情報, ブロック段情報リスト, 管理情報);
			break;
		case ゲームオーバー:
			ゲームオーバーを描く();
			break;
		case クリア:
			クリアを描く();
		default:
			break;
		}

		ゲーム描画(g);

		g.drawImage(管理情報.バッファ, 0, 0, this);
	}

	/**
	 * ゲーム描画
	 *
	 * @param g
	 */
	private void ゲーム描画(Graphics g) {
		ブロックの制御.描写(管理情報, ブロック段情報リスト);
		ボールの制御.描写(ボール情報, 管理情報);
		バーの制御.描写(バー情報, 管理情報);
	}

	/**
	 * ゲーム開始を描く
	 */
	private void ゲーム開始を描く() {
		管理情報.グラフィック.setColor(Color.ORANGE);
		管理情報.グラフィック.drawString(定数情報.ゲーム開始文言, (管理情報.移動範囲_幅 / 2)
				- (定数情報.ゲーム開始文言サイズ * 2), 管理情報.移動範囲_高さ);
	}

	/**
	 * ゲームオーバーを描く
	 */
	private void ゲームオーバーを描く() {
		管理情報.グラフィック.setColor(Color.RED);
		管理情報.グラフィック.drawString(定数情報.ゲームオーバー文言, (管理情報.移動範囲_幅 / 2)
				- (定数情報.ゲームオーバー文言サイズ * 2), 管理情報.移動範囲_高さ);
	}

	/**
	 * クリアを描く
	 */
	private void クリアを描く() {
		管理情報.グラフィック.setColor(Color.YELLOW);
		管理情報.グラフィック.drawString(定数情報.クリア文言, (管理情報.移動範囲_幅 / 2)
				- (定数情報.クリア文言サイズ * 2), 管理情報.移動範囲_高さ);
	}

	@Override
	public void start() {
		if (管理情報.スレッド == null) {
			管理情報.スレッド = new Thread(this);
			管理情報.スレッド.start();
		}
	}

	@Override
	public void stop() {
		管理情報.スレッド = null;
	}

	public void run() {
		while (true) {
			repaint();
			try {
				Thread.sleep(管理情報.スピード.スピード);
			} catch (InterruptedException e) {
			}
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyChar()) {
		case 定数情報.左移動キー:
			バーの制御.制御(バー情報, 管理情報, 移動方向の種類.左);
			if (!ゲームの進行.ゲーム中.equals(管理情報.ゲームの進行情報)) {
				ボールの制御.バーと並行移動(ボール情報, バー情報);
			}
			break;
		case 定数情報.右移動キー:
			バーの制御.制御(バー情報, 管理情報, 移動方向の種類.右);
			if (!ゲームの進行.ゲーム中.equals(管理情報.ゲームの進行情報)) {
				ボールの制御.バーと並行移動(ボール情報, バー情報);
			}
			break;
		case 定数情報.スタートキー:
			if (管理情報.ゲームの進行情報.equals(ゲームの進行.起動)) {
				管理情報.ゲームの進行情報 = ゲームの進行.ゲーム中;
			} else if (管理情報.ゲームの進行情報.equals(ゲームの進行.ゲームオーバー)) {
				再起動();
			} else if (管理情報.ゲームの進行情報.equals(ゲームの進行.クリア)) {
				再起動();
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		switch (e.getKeyChar()) {
		case 定数情報.左移動キー:
			バー情報.押しっぱなしか = false;
			バー情報.移動方向 = ベクトルの種類.ゼロ;
			break;
		case 定数情報.右移動キー:
			バー情報.押しっぱなしか = false;
			バー情報.移動方向 = ベクトルの種類.ゼロ;
			break;
		default:
			break;
		}
	}
}