package com.breakOut.constants;

import java.awt.Color;
import java.awt.event.KeyEvent;

public class 定数情報 {

	//++++++++++++++++++++++++++++++
	// 画面
	//++++++++++++++++++++++++++++++

	public static final int 画面の幅 = 500;

	public static final int 画面の高さ = 500;

	public static final int マージン = 5;

	public static final Color 外枠色 = Color.LIGHT_GRAY;

	public static final Color 内枠色 = Color.BLACK;

	//++++++++++++++++++++++++++++++
	// ボール
	//++++++++++++++++++++++++++++++

	public static final int ボールの幅 = 10;

	public static final int ボールの高さ = 10;

	public static final int スタート時のボール角度 = 45;

	// FIXME
	public static final int ボールの移動速度 = 4;

	public static final int ボール移動角度の最少 = 10;

	public static final int 移動中バー衝突時のボール角度補正角度 = 15;

	public static final int ボール衝突範囲補正値 = 1;

	//++++++++++++++++++++++++++++++
	// バー
	//++++++++++++++++++++++++++++++

	// FIXME
	public static final int バーの幅 = 60;

	public static final int バーの高さ = 10;

	public static final int バーの底からのマージン = 40;

	// FIXME
	public static final int バーの移動速度 = 5;

	//++++++++++++++++++++++++++++++
	// ブロック
	//++++++++++++++++++++++++++++++

	// FIXME
	public static final int ブロックの幅 = 16;

	// FIXME
	public static final int ブロックの高さ = 16;

	public static final int ブロック間隔_幅 = 3;

	public static final int ブロック間隔_高さ = 3;

	public static final int ブロック設置の開始位置X = 23;

	public static final int ブロック設置の開始位置Y = 23;

	//++++++++++++++++++++++++++++++
	// キー
	//++++++++++++++++++++++++++++++

	public static final char 左移動キー = KeyEvent.VK_1;

	public static final char 右移動キー = KeyEvent.VK_0;

	public static final char スタートキー = KeyEvent.VK_ENTER;

	//++++++++++++++++++++++++++++++
	// ゲーム文言
	//++++++++++++++++++++++++++++++

	public static final String ゲーム開始文言 = "ゲーム開始[ENTER]　／　←[" + 左移動キー + "]　／　→[" + 右移動キー + "]";

	public static final int ゲーム開始文言サイズ = ゲーム開始文言.getBytes().length;

	public static final String ゲームオーバー文言 = "ゲームオーバー[ENTER]";

	public static final int ゲームオーバー文言サイズ = ゲームオーバー文言.getBytes().length;

	public static final String クリア文言 = "ゲームクリア！！！[ENTER]";

	public static final int クリア文言サイズ = クリア文言.getBytes().length;

	//++++++++++++++++++++++++++++++
	// ゲームオーバー
	//++++++++++++++++++++++++++++++

	// FIXME
	public static final boolean ゲームオーバーあり = false;
}
