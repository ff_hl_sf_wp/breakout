package com.breakOut.model;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

import com.breakOut.enums.ゲームの進行;
import com.breakOut.enums.スピードの種類;


public class 管理情報 {

	public スピードの種類 スピード = スピードの種類.普通;

	public Thread スレッド = null;

	public Dimension 表示領域;

	public Image バッファ;

	public Graphics グラフィック;

	public int 移動範囲_幅 = 0;

	public int 移動範囲_高さ = 0;

	public ゲームの進行 ゲームの進行情報 = null;

	public int 左の移動制限X = 0;

	public int 上の移動制限Y = 0;

	public int 右の移動制限X = 0;

	public int 下の移動制限Y = 0;

	public int ブロックの衝突範囲X1 = 0;

	public int ブロックの衝突範囲X2 = 0;

	public int ブロックの衝突範囲Y1 = 0;

	public int ブロックの衝突範囲Y2 = 0;

	public boolean クリアありか = false;

	public int 残ブロック数 = 0;
}
