package com.breakOut.model;

import com.breakOut.enums.ベクトルの種類;


public class ボール {

	public int 座標X;

	public double 座標X_計算用;

	public int 座標Y;

	public double 座標Y_計算用;

	public ベクトルの種類 ベクトルX;

	public ベクトルの種類 ベクトルY;

	public int 角度;

	public double コサイン;

	public double サイン;

	public boolean 衝突したか;
}
