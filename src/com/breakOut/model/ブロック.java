package com.breakOut.model;

import java.awt.Color;
import java.util.List;

import com.breakOut.enums.ブロックの状態;
import com.breakOut.enums.ブロックの種類;

public class ブロック {

	public ブロックの種類 種類;

	public Color 色;

	public ブロックの状態 状態;

	public int 破壊までの回数;

	public int 座標X;

	public int 座標Y;

	public List<ひび割れ> ひび割れリスト;
}
