package com.breakOut.control;

import java.awt.Color;

import com.breakOut.constants.定数情報;
import com.breakOut.enums.ベクトルの種類;
import com.breakOut.enums.移動方向の種類;
import com.breakOut.model.バー;
import com.breakOut.model.管理情報;

public class バーの制御 {

	/**
	 * 初期化
	 */
	public static void 初期化(バー バー情報, 管理情報 管理情報) {
		バー情報.座標X = 管理情報.移動範囲_幅 / 2 - ((定数情報.バーの幅 / 2) - (定数情報.ボールの幅 / 2));
		バー情報.座標Y = 管理情報.移動範囲_高さ - 定数情報.バーの底からのマージン;
		バー情報.押しっぱなしか = false;
		バー情報.移動方向 = ベクトルの種類.ゼロ;
		バー情報.移動速度 = 定数情報.バーの移動速度;
	}

	/**
	 * 描写
	 */
	public static void 描写(バー バー情報, 管理情報 管理情報) {
		管理情報.グラフィック.setColor(Color.WHITE);
		管理情報.グラフィック.fill3DRect(バー情報.座標X, バー情報.座標Y, 定数情報.バーの幅, 定数情報.バーの高さ, true);
	}

	public static void 制御(バー バー情報, 管理情報 管理情報, 移動方向の種類 移動方向) {

		int 左の移動制限 = 定数情報.マージン;
		int 右の移動制限 = 定数情報.マージン + 管理情報.移動範囲_幅 - 定数情報.バーの幅;
		int 仮 = 0;

		switch (移動方向) {
		case 左:
			if (バー情報.座標X > 左の移動制限) {
				仮 = バー情報.座標X - バー情報.移動速度;
				if (仮 < 左の移動制限) {
					バー情報.座標X = 左の移動制限;
				} else {
					バー情報.座標X = 仮;
				}
				バー情報.押しっぱなしか = true;
				バー情報.移動方向 = ベクトルの種類.マイナス;
			}
			break;
		case 右:
			if (バー情報.座標X < 右の移動制限) {
				仮 = バー情報.座標X + バー情報.移動速度;
				if (仮 > 右の移動制限) {
					バー情報.座標X = 右の移動制限;
				} else {
					バー情報.座標X = 仮;
				}
				バー情報.押しっぱなしか = true;
				バー情報.移動方向 = ベクトルの種類.プラス;
			}
			break;
		default:
			break;
		}
	}
}
