package com.breakOut.control;

import com.breakOut.constants.定数情報;
import com.breakOut.model.管理情報;

public class 背景の制御 {

	/**
	 * 描写
	 */
	public static void 描写(管理情報 管理情報) {
		管理情報.グラフィック.setColor(定数情報.外枠色);
		管理情報.グラフィック.fillRect(0, 0, 管理情報.表示領域.width, 管理情報.表示領域.height);
		管理情報.グラフィック.setColor(定数情報.内枠色);
		管理情報.グラフィック.fillRect(定数情報.マージン, 定数情報.マージン, 管理情報.表示領域.width
				- (定数情報.マージン * 2), 管理情報.表示領域.height - (定数情報.マージン * 2));
	}

}
