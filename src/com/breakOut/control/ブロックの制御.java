package com.breakOut.control;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.breakOut.constants.定数情報;
import com.breakOut.enums.ブロックの状態;
import com.breakOut.enums.ブロックの種類;
import com.breakOut.enums.方向の種類;
import com.breakOut.model.ひび割れ;
import com.breakOut.model.ブロック;
import com.breakOut.model.ボール;
import com.breakOut.model.管理情報;

public class ブロックの制御 {

	/**
	 * ブロック生成_なし
	 *
	 * @return
	 */
	public static ブロック ブロック生成_なし() {
		ブロック ブロック情報 = new ブロック();
		ブロック情報.種類 = ブロックの種類.なし;
		ブロック情報.状態 = ブロックの状態.非表示;
		return ブロック情報;
	}

	/**
	 * ブロック生成_破壊不能
	 *
	 * @return
	 */
	public static ブロック ブロック生成_破壊不能() {
		ブロック ブロック情報 = new ブロック();
		ブロック情報.種類 = ブロックの種類.破壊不能;
		ブロック情報.色 = Color.GRAY;
		ブロック情報.状態 = ブロックの状態.表示;
		return ブロック情報;
	}

	/**
	 * ブロック生成_通常
	 *
	 * @param 色
	 * @param 破壊までの回数
	 * @return
	 */
	public static ブロック ブロック生成_通常(Color 色, int 破壊までの回数) {
		ブロック ブロック情報 = new ブロック();
		ブロック情報.種類 = ブロックの種類.通常;
		ブロック情報.色 = 色;
		ブロック情報.状態 = ブロックの状態.表示;
		ブロック情報.破壊までの回数 = 破壊までの回数;
		ブロック情報.ひび割れリスト = new ArrayList<ひび割れ>();
		return ブロック情報;
	}

	/**
	 * ブロックの衝突範囲の設定
	 *
	 * @param 管理情報
	 * @param ブロック段情報リスト
	 */
	public static void ブロックの衝突範囲の設定(管理情報 管理情報, List<List<ブロック>> ブロック段情報リスト) {

		int 段数 = ブロック段情報リスト.size();
		int 最大列数 = 0;
		for (List<ブロック> ブロック情報リスト : ブロック段情報リスト) {
			if (最大列数 < ブロック情報リスト.size()) {
				最大列数 = ブロック情報リスト.size();
			}
		}

		管理情報.ブロックの衝突範囲X1 = 定数情報.ブロック設置の開始位置X;
		管理情報.ブロックの衝突範囲X2 = 定数情報.ブロック設置の開始位置X + (定数情報.ブロックの幅 * 最大列数)
				+ (定数情報.ブロック間隔_幅 * (最大列数 - 1));
		管理情報.ブロックの衝突範囲Y1 = 定数情報.ブロック設置の開始位置Y;
		管理情報.ブロックの衝突範囲Y2 = 定数情報.ブロック設置の開始位置Y + (定数情報.ブロックの高さ * 段数)
				+ (定数情報.ブロック間隔_高さ * (段数 - 1));
	}

	/**
	 * 座標設定
	 *
	 * @param 管理情報
	 * @param ブロック段情報リスト
	 */
	public static void 座標設定(管理情報 管理情報, List<List<ブロック>> ブロック段情報リスト) {

		int 残ブロック数 = 0;

		for (int 段 = 0; 段 < ブロック段情報リスト.size(); 段++) {

			List<ブロック> ブロック段情報 = ブロック段情報リスト.get(段);

			for (int ブロック数 = 0; ブロック数 < ブロック段情報.size(); ブロック数++) {

				ブロック ブロック情報 = ブロック段情報.get(ブロック数);

				ブロック情報.座標X = 定数情報.ブロック設置の開始位置X + (定数情報.ブロック間隔_幅 * ブロック数)
						+ (定数情報.ブロックの幅 * ブロック数);
				ブロック情報.座標Y = 定数情報.ブロック設置の開始位置Y + (定数情報.ブロック間隔_高さ * 段)
						+ (定数情報.ブロックの高さ * 段);

				if (ブロックの種類.通常.equals(ブロック情報.種類)) {
					残ブロック数 = 残ブロック数 + 1;
				}
			}
		}

		if (残ブロック数 > 0) {
			管理情報.クリアありか = true;
		}

		管理情報.残ブロック数 = 残ブロック数;
	}

	/**
	 * ひび割れ設定
	 *
	 * @param ブロック情報
	 */
	public static void ひび割れ設定(ブロック ブロック情報, ボール ボール情報, 方向の種類 方向) {

		if (ブロックの種類.通常.equals(ブロック情報.種類)) {
			ブロック情報.破壊までの回数 = ブロック情報.破壊までの回数 - 1;
			if (ブロック情報.破壊までの回数 >= 0) {
				ブロック情報.状態 = ブロックの状態.ひび割れ;
			} else {
				ブロック情報.状態 = ブロックの状態.非表示;
			}
		} else {
			return;
		}

		Random ランダム = new Random();
		ひび割れ ひび割れ情報 = new ひび割れ();

		int ひび割れ_範囲_幅 = 0;
		int ひび割れ_範囲_長さ = 0;
		int ひび割れ_開始X = 0;
		int ひび割れ_開始Y = 0;

		switch (方向) {
		case 左:
			ひび割れ_範囲_幅 = 定数情報.ブロックの高さ / 2;
			ひび割れ_範囲_長さ = 定数情報.ブロックの高さ;
			ひび割れ_開始X = ブロック情報.座標X + 定数情報.ブロックの幅;
			ひび割れ_開始Y = ボール情報.座標Y + (定数情報.ボールの高さ / 2);
			break;
		case 上:
			ひび割れ_範囲_幅 = 定数情報.ブロックの幅 / 4;
			ひび割れ_範囲_長さ = 定数情報.ブロックの高さ;
			ひび割れ_開始X = ボール情報.座標X + (定数情報.ボールの幅 / 2);
			ひび割れ_開始Y = ブロック情報.座標Y + 定数情報.ブロックの高さ;
			break;
		case 右:
			ひび割れ_範囲_幅 = 定数情報.ブロックの高さ / 2;
			ひび割れ_範囲_長さ = 定数情報.ブロックの高さ;
			ひび割れ_開始X = ブロック情報.座標X;
			ひび割れ_開始Y = ボール情報.座標Y + (定数情報.ボールの高さ / 2);
			break;
		case 下:
			ひび割れ_範囲_幅 = 定数情報.ブロックの幅 / 4;
			ひび割れ_範囲_長さ = 定数情報.ブロックの高さ;
			ひび割れ_開始X = ボール情報.座標X + (定数情報.ボールの幅 / 2);
			ひび割れ_開始Y = ブロック情報.座標Y;
			break;
		default:
			break;
		}

		int ひび割れ_中継X = 0;
		int ひび割れ_中継Y = 0;
		int ひび割れ_終了X = 0;
		int ひび割れ_終了Y = 0;

		int 中継_方向 = ランダム.nextInt(2);
		int 終了_方向 = ランダム.nextInt(2);

		int 中継_移動値 = ランダム.nextInt(ひび割れ_範囲_幅);
		int 終了_移動値 = ランダム.nextInt(ひび割れ_範囲_幅);

		int 中継_長さ = ランダム.nextInt(ひび割れ_範囲_長さ);
		int 終了_長さ = ひび割れ_範囲_長さ - 中継_長さ;

		switch (方向) {
		case 左:
			ひび割れ_中継Y = ひび割れの方向補正(中継_方向, ひび割れ_開始Y, 中継_移動値, ブロック情報.座標Y
					+ 定数情報.ブロックの高さ - 1, ブロック情報.座標Y + 1, 方向の種類.左);
			ひび割れ_終了Y = ひび割れの方向補正(終了_方向, ひび割れ_中継Y, 終了_移動値, ブロック情報.座標Y
					+ 定数情報.ブロックの高さ - 1, ブロック情報.座標Y + 1, 方向の種類.左);
			ひび割れ_中継X = ブロック情報.座標X + 定数情報.ブロックの幅 - 中継_長さ;
			ひび割れ_終了X = ひび割れ_中継X - 終了_長さ;
			break;
		case 上:
			ひび割れ_中継X = ひび割れの方向補正(中継_方向, ひび割れ_開始X, 中継_移動値, ブロック情報.座標X + 1,
					ブロック情報.座標X + 定数情報.ブロックの高さ - 1, 方向の種類.上);
			ひび割れ_終了X = ひび割れの方向補正(終了_方向, ひび割れ_中継X, 終了_移動値, ブロック情報.座標X + 1,
					ブロック情報.座標X + 定数情報.ブロックの高さ - 1, 方向の種類.上);
			ひび割れ_中継Y = ブロック情報.座標Y + 定数情報.ブロックの高さ - 中継_長さ;
			ひび割れ_終了Y = ひび割れ_中継Y - 終了_長さ;
			break;
		case 右:
			ひび割れ_中継Y = ひび割れの方向補正(中継_方向, ひび割れ_開始Y, 中継_移動値, ブロック情報.座標Y + 1,
					ブロック情報.座標Y + 定数情報.ブロックの高さ - 1, 方向の種類.右);
			ひび割れ_終了Y = ひび割れの方向補正(終了_方向, ひび割れ_中継Y, 終了_移動値, ブロック情報.座標Y + 1,
					ブロック情報.座標Y + 定数情報.ブロックの高さ - 1, 方向の種類.右);
			ひび割れ_中継X = ブロック情報.座標X + 中継_長さ;
			ひび割れ_終了X = ひび割れ_中継X + 終了_長さ;
			break;
		case 下:
			ひび割れ_中継X = ひび割れの方向補正(中継_方向, ひび割れ_開始X, 中継_移動値, ブロック情報.座標X
					+ 定数情報.ブロックの高さ - 1, ブロック情報.座標X + 1, 方向の種類.下);
			ひび割れ_終了X = ひび割れの方向補正(終了_方向, ひび割れ_中継X, 終了_移動値, ブロック情報.座標X
					+ 定数情報.ブロックの高さ - 1, ブロック情報.座標X + 1, 方向の種類.下);
			ひび割れ_中継Y = ブロック情報.座標Y + 中継_長さ;
			ひび割れ_終了Y = ひび割れ_中継Y + 終了_長さ;
			break;
		default:
			break;
		}

		ひび割れ情報.ひび割れ_開始X = ひび割れ_開始X;
		ひび割れ情報.ひび割れ_開始Y = ひび割れ_開始Y;
		ひび割れ情報.ひび割れ_中継X = ひび割れ_中継X;
		ひび割れ情報.ひび割れ_中継Y = ひび割れ_中継Y;
		ひび割れ情報.ひび割れ_終了X = ひび割れ_終了X;
		ひび割れ情報.ひび割れ_終了Y = ひび割れ_終了Y;

		ブロック情報.ひび割れリスト.add(ひび割れ情報);
	}

	/**
	 * ひび割れの方向補正
	 *
	 * @param X方向
	 * @param 移動元X
	 * @param 移動値X
	 * @param 制限_左
	 * @param 制限_右
	 * @param 方向
	 * @return
	 */
	private static int ひび割れの方向補正(int X方向, int 移動元X, int 移動値X, int 制限_左,
			int 制限_右, 方向の種類 方向) {

		int 座標X = 0;

		switch (方向) {
		case 左:
			// 下向き
			if (X方向 == 0) {
				if (移動元X + 移動値X <= 制限_左) {
					座標X = 移動元X + 移動値X;
				} else {
					座標X = 移動元X - 移動値X;
				}
				// 上向き
			} else {
				if (移動元X - 移動値X >= 制限_右) {
					座標X = 移動元X - 移動値X;
				} else {
					座標X = 移動元X + 移動値X;
				}
			}
			break;
		case 上:
			// 左向き
			if (X方向 == 0) {
				if (移動元X - 移動値X >= 制限_左) {
					座標X = 移動元X - 移動値X;
				} else {
					座標X = 移動元X + 移動値X;
				}
				// 右向き
			} else {
				if (移動元X + 移動値X <= 制限_右) {
					座標X = 移動元X + 移動値X;
				} else {
					座標X = 移動元X - 移動値X;
				}
			}
			break;
		case 右:
			// 上向き
			if (X方向 == 0) {
				if (移動元X - 移動値X >= 制限_左) {
					座標X = 移動元X - 移動値X;
				} else {
					座標X = 移動元X + 移動値X;
				}
				// 下向き
			} else {
				if (移動元X + 移動値X <= 制限_右) {
					座標X = 移動元X + 移動値X;
				} else {
					座標X = 移動元X - 移動値X;
				}
			}
			break;
		case 下:
			// 右向き
			if (X方向 == 0) {
				if (移動元X + 移動値X <= 制限_左) {
					座標X = 移動元X + 移動値X;
				} else {
					座標X = 移動元X - 移動値X;
				}
				// 左向き
			} else {
				if (移動元X - 移動値X >= 制限_右) {
					座標X = 移動元X - 移動値X;
				} else {
					座標X = 移動元X + 移動値X;
				}
			}
			break;
		default:
			break;
		}

		return 座標X;
	}

	/**
	 * 描写
	 *
	 * @param 管理情報
	 * @param ブロック段情報リスト
	 */
	public static void 描写(管理情報 管理情報, List<List<ブロック>> ブロック段情報リスト) {

		for (int 段 = 0; 段 < ブロック段情報リスト.size(); 段++) {

			List<ブロック> ブロック段情報 = ブロック段情報リスト.get(段);

			for (int ブロック数 = 0; ブロック数 < ブロック段情報.size(); ブロック数++) {

				ブロック ブロック情報 = ブロック段情報.get(ブロック数);

				描写_ブロック１個(管理情報, ブロック情報);
			}
		}
	}

	/**
	 * 描写_ブロック１個
	 *
	 * @param 管理情報
	 * @param ブロック情報
	 */
	private static void 描写_ブロック１個(管理情報 管理情報, ブロック ブロック情報) {

		if (ブロックの種類.なし.equals(ブロック情報.種類)) {
			return;
		}

		if (ブロックの状態.非表示.equals(ブロック情報.状態)) {
			return;
		}

		管理情報.グラフィック.setColor(ブロック情報.色);
		管理情報.グラフィック.fill3DRect(ブロック情報.座標X, ブロック情報.座標Y, 定数情報.ブロックの幅,
				定数情報.ブロックの高さ, true);

		if (ブロックの状態.ひび割れ.equals(ブロック情報.状態)) {
			描写_ひび割れ(管理情報, ブロック情報);
		}
	}

	/**
	 * 描写_ひび割れ
	 *
	 * @param 管理情報
	 * @param ブロック情報
	 */
	private static void 描写_ひび割れ(管理情報 管理情報, ブロック ブロック情報) {

		for (ひび割れ ひび割れ情報 : ブロック情報.ひび割れリスト) {
			管理情報.グラフィック.setColor(定数情報.内枠色);
			管理情報.グラフィック.drawLine(ひび割れ情報.ひび割れ_開始X, ひび割れ情報.ひび割れ_開始Y,
					ひび割れ情報.ひび割れ_中継X, ひび割れ情報.ひび割れ_中継Y);
			管理情報.グラフィック.drawLine(ひび割れ情報.ひび割れ_中継X, ひび割れ情報.ひび割れ_中継Y,
					ひび割れ情報.ひび割れ_終了X, ひび割れ情報.ひび割れ_終了Y);
		}
	}
}
